<?hh //partial
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/

namespace nuclio\plugin\application\responder
{
	use nuclio\core\plugin\Plugin;
	use nuclio\core\ClassManager;
	use nuclio\helper\MimeHelper;
	use nuclio\plugin\application\responder\ResponderException;


	class Responder extends Plugin
	{
		const DEFAULT_TYPE		='txt';
		const DEFAULT_HANDLER	='txt';
		const HANDLER_PREFIX	='handler_';
		
		private $type		=null;
		private $mimeType	=null;
		private $headers	=[];
		private $data		=null;
		private $download	=false;
		
		public static function getInstance(/* HH_FIXME[4033] */ ...$args):Responder
		{
			$instance=ClassManager::getClassInstance(self::class,...$args);
			return ($instance instanceof self)?$instance:new self(...$args);
		}

		public function __construct($type=self::DEFAULT_TYPE)
		{
			parent::__construct();
			$this->setType($type);
		}
		
		public function setType($type=self::DEFAULT_TYPE)
		{
			$this->type	=$type;
			$mimeType	=MimeHelper::getMimeTypeFromExtension($type);
			$this->addHeader('content-type',$mimeType);
		}
		
		public function setData($data=null)
		{
			$this->data=$data;
			return $this;
		}
		
		private function handler_txt()
		{
			print $this->data;
		}
		
		private function handler_json()
		{
			print json_encode($this->data);
		}
		
		private function handler_debug()
		{
			var_dump($this->data);
		}
		
		public function send()
		{
			header("Access-Control-Allow-Origin: *");
			foreach ($this->headers as $key=>$val)
			{
				header($key.':'.$val.';');
			}
			if ($this->download)
			{
				header('Content-Disposition: attachment; filename="'.$this->download.'"');
			}
			$method=self::HANDLER_PREFIX.$this->type;
			if (method_exists($this,$method))
			{
				call_user_func_array([$this,$method], []);
			}
			else
			{
				$handler=self::HANDLER_PREFIX.self::DEFAULT_HANDLER;
				$this->$handler();
			}
			if ($this->download)
			{
				exit();
			}
			return $this;
		}
		
		public function addHeader($key,$val)
		{
			$this->headers[$key]=$val;
			return $this;
		}
		
		public function forceDownload($fileName)
		{
			$this->download=$fileName;
			return $this;
		}
	}
}
